## check for new chart releases here: https://artifacthub.io/packages/helm/cert-manager/cert-manager

helm upgrade --create-namespace -i --repo https://charts.jetstack.io -n cert-manager cert-manager cert-manager -f `dirname "$0"`/values.yaml --version 1.11.1
