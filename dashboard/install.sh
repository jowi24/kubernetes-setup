#!/bin/bash

echo -n "Enter domain dashboard should be reachable: "
read HOSTNAME
export HOSTNAME=$HOSTNAME

envsubst < `dirname "$0"`/values.yaml > `dirname "$0"`/values-env.yaml

# check for chart updates at https://artifacthub.io/packages/helm/k8s-dashboard/kubernetes-dashboard
helm upgrade -i --create-namespace --repo https://kubernetes.github.io/dashboard/ -n kubernetes-dashboard kubernetes-dashboard kubernetes-dashboard -f `dirname "$0"`/values-env.yaml --version 7.4.0

kubectl apply -f `dirname "$0"`/admin-account.yaml

kubectl delete secret ca-secret --namespace=kubernetes-dashboard
kubectl create secret generic ca-secret --from-file=ca.crt=`dirname "$0"`/../yubikey-ca.crt --namespace=kubernetes-dashboard

echo "Use the following login token, to authenticate:"
kubectl -n kubernetes-dashboard create token admin-user --duration=8760h
