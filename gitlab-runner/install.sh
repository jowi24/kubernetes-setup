#!/bin/bash

echo -n "Enter gitlab runner token: "
read TOKEN
export TOKEN=$TOKEN

envsubst < `dirname "$0"`/values.yaml > `dirname "$0"`/values-env.yaml

helm upgrade --create-namespace -i --repo https://charts.gitlab.io --namespace gitlab-runner gitlab-runner -f `dirname "$0"`/values-env.yaml gitlab-runner 
