# check for new chart releases here: https://github.com/kubernetes/ingress-nginx

helm upgrade --create-namespace -i --repo https://kubernetes.github.io/ingress-nginx -n kube-ingress ingress-nginx ingress-nginx -f `dirname "$0"`/values.yaml --version 4.6.0
