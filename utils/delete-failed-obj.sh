#!/bin/bash

# Removes all failed job and pod objects

kubectl delete jobs --all-namespaces --field-selector 'status.successful==0' 
kubectl delete po   --all-namespaces --field-selector 'status.phase==Failed'