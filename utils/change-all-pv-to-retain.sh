#!/bin/bash

# Patches all persistent volumes to a "retain" reclaim policy

kubectl get pv | cut -d\  -f 1 | xargs -i kubectl patch pv {} -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'
