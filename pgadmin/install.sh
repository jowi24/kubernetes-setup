#!/bin/bash

helm upgrade --create-namespace -i --repo https://helm.runix.net/ --namespace pgadmin pgadmin4 -f `dirname "$0"`/values.yaml pgadmin4

kubectl delete secret ca-secret --namespace=pgadmin

kubectl create secret generic ca-secret --from-file=ca.crt=`dirname "$0"`/../yubikey-ca.crt --namespace=pgadmin
