# About this repository

Scripts to support set-up of a bare-metal kubernetes cluster, see https://joachim-wilke.de/blog/2020/04/14/Kubernetes-Single-Node-Cluster-aufsetzen/ for more details (in German only).
